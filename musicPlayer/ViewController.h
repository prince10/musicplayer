//
//  ViewController.h
//  musicPlayer
//
//  Created by Prince on 27/11/15.
//  Copyright (c) 2015 Prince. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import <MediaPlayer/MediaPlayer.h>
@interface ViewController : UIViewController<UITableViewDelegate,UITableViewDataSource>


@end

