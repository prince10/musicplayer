//
//  musicViewController.m
//  musicPlayer
//
//  Created by Prince on 27/11/15.
//  Copyright (c) 2015 Prince. All rights reserved.
//

#import "musicViewController.h"



@interface musicViewController ()
{
     AVAudioPlayer *music;
    NSArray *songs;
    NSString *path;
    NSURL *songsURl;
    NSBundle *paths;
    IBOutlet UILabel *volumeLbl;
    
    IBOutlet UILabel *musicLabel;
    IBOutlet UISlider *volumeSlider;
    
    IBOutlet UISlider *musicSlider;
}
@property (strong, nonatomic) IBOutlet UIImageView *theamView;
@property (strong, nonatomic) IBOutlet UIButton *backButton;
@property (strong, nonatomic) IBOutlet UIButton *pauseAndPlayButton;
@property (strong, nonatomic) IBOutlet UIButton *nextButton;


@end

@implementation musicViewController
@synthesize pauseAndPlayButton;
@synthesize theamView;

int i;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    
    songs =[[NSArray alloc]initWithObjects:@"04 Neendein Khul Jaati Hain (Hate Story 3) Mika n Kanika 190Kbps",@"Time table",@"Outfit",@"Salute - Bohemia 190Kbps",@"Taur (Bohemia n Gippy Grewal) 320kbps" ,@"01 Tumhe Apna Banane Ka (Hate Story 3) Armaan Malik 190Kbps",@"Tum Saath Ho - Tamasha (Arijit Singh) 190Kbps",@"Mere Baare",@"Main_Rahoon_Ya_Na_Rahoon_TopGaana.com",@"The Sufi Mashup (By Kiran Kamath) (SongsMp3.Com)",@"Mainu Ishq Da Lagya Rog (Tulsi Kumar) (SongsMp3.Com)",@"01 - Dil Dola Re (Angry Indian Goddesses) (SongsMp3.Com)",@"Taur-The-PropheC (SongsMp3.Com)",nil];
    
    path=[NSString stringWithFormat:@"%@/%@.mp3",[[NSBundle mainBundle] resourcePath],songs[0]];
    
    
    songsURl = [[NSURL alloc] initFileURLWithPath: path];
    
    
   music = [[AVAudioPlayer alloc]initWithContentsOfURL:songsURl error:nil];
    
    
    
    // Do any additional setup after loading the view.
    
    
}



- (IBAction)playAndPushButtonPressed:(id)sender
{
    
      if ([music isPlaying]==NO)
      
      {
          
          
          
    [pauseAndPlayButton setImage:[UIImage imageNamed:@"playButton.png"] forState:UIControlStateNormal];
          
          [music play];
          
         pauseAndPlayButton.selected=YES;
          
  }
    
    
      else
          
      {
          [music pause];
           pauseAndPlayButton.selected=NO;
          
          [pauseAndPlayButton setImage:[UIImage imageNamed:@"pauseButton.png"] forState:UIControlStateNormal];
         
      }




        
}


- (IBAction)nextButtonPressed:(id)sender

{
    
    if(i==songs.count)
    {
    
        [music play];
        i++;
   }
    else
     
    {
        i++;
        musicSlider.maximumValue=0;

        [music play];
    
        path=[NSString stringWithFormat:@"%@/%@.mp3",[[NSBundle mainBundle] resourcePath],songs[i]];
    
    
        songsURl = [[NSURL alloc] initFileURLWithPath: path];
    
        music = [[AVAudioPlayer alloc]initWithContentsOfURL:songsURl error:nil];
        [music play];
        

    }
    
    
  if (i==0) {
        
        theamView.image = [UIImage imageNamed:@"neendeinkhul"];

        
 }
    else if (i==1)
    {
        theamView.image = [UIImage imageNamed:@"Time-Table-2"];
    

    }
    else if (i==2)
    {
        theamView.image = [UIImage imageNamed:@"outfit"];


    }
    else if (i==3)
    {
        theamView.image = [UIImage imageNamed:@"salute"];

        
    }
    else if (i==4)
    {
        theamView.image = [UIImage imageNamed:@"taur"];
        
        
    }
    else if (i==5)
    {
        theamView.image = [UIImage imageNamed:@"hatestory3"];
        
        
    }
    else if (i==6)
    {
        theamView.image = [UIImage imageNamed:@"Agar-Tum-"];
        
        
    }
    else if (i==7)
    {
        theamView.image = [UIImage imageNamed:@"Mere-Bare-Bohemia"];
        
    
    }
    
    else if (i==8)
    {
        theamView.image = [UIImage imageNamed:@"main rahun na rahun"];
        
        
    }
    else if (i==9)
    {
        theamView.image = [UIImage imageNamed:@"sufi"];
        
        
    }
    else if (i==10)
    {
        theamView.image = [UIImage imageNamed:@"Mainu Ishq"];
        
        
    }
    else if (i==11)
    {
        theamView.image = [UIImage imageNamed:@"Angry Indian"];
        
        
    }
    else if (i==12)
    {
        theamView.image = [UIImage imageNamed:@"propca"];
        i=0;
        
    }
    
  
   

   
}

- (IBAction)backButtonPressed:(id)sender
{
    
    if(i==0)
    {
        [music play];
    }
    else{
        i--;
        [music stop];
        
        path=[NSString stringWithFormat:@"%@/%@.mp3",[[NSBundle mainBundle] resourcePath],songs[i]];
        
        
        songsURl = [[NSURL alloc] initFileURLWithPath: path];
        
        music = [[AVAudioPlayer alloc]initWithContentsOfURL:songsURl error:nil];
        [music play];
        
        
        
        
        if (i==12) {
            
            theamView.image = [UIImage imageNamed:@"propca"];
            
            
        }
        else if (i==11)
        {
            theamView.image = [UIImage imageNamed:@"Angry Indian"];
            
            
        }
        else if (i==10)
        {
            theamView.image = [UIImage imageNamed:@"Mainu Ishq"];
            
            
        }
        else if (i==9)
        {
            theamView.image = [UIImage imageNamed:@"sufi"];
            
            
        }
        else if (i==8)
        {
            theamView.image = [UIImage imageNamed:@"main rahun na rahun"];
            
            
        }
        
        
        
        
        if (i==7) {
            
            theamView.image = [UIImage imageNamed:@"Mere-Bare-Bohemia"];
         
            
        }
        else if (i==6)
        {
            theamView.image = [UIImage imageNamed:@"Agar-Tum-"];
         
            
        }
        else if (i==5)
        {
            theamView.image = [UIImage imageNamed:@"hatestory3"];
           }
        else if (i==4)
        {
            theamView.image = [UIImage imageNamed:@"taur"];
        }

        else if (i==3)
        {
            theamView.image = [UIImage imageNamed:@"salute"];
        }

        else if (i==2)
        {
            theamView.image = [UIImage imageNamed:@"outfit"];
        }

        else if (i==1)
        {
            theamView.image = [UIImage imageNamed:@"Time-Table-2"];
        }
        else if (i==0)
        {
            theamView.image = [UIImage imageNamed:@"neendeinkhul"];
            i=12;
        }
       
    }

    
}


- (IBAction)volumeSliderAction:(id)sender {
    
    
    music.volume=volumeSlider.value;
    
}


- (IBAction)musicSliderAction:(id)sender {
   
    [music setCurrentTime : musicSlider.value];
    [music prepareToPlay];
    musicSlider.maximumValue=[music duration];
    
    
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
