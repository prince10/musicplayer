//
//  ViewController.m
//  musicPlayer
//
//  Created by Prince on 27/11/15.
//  Copyright (c) 2015 Prince. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

{
    NSArray *songsArray;
    AVAudioPlayer *music;
 NSMutableArray *positions;
    
}
@property (strong, nonatomic) IBOutlet UITableView *musicListTableView;

@end

@implementation ViewController
@synthesize musicListTableView;


- (void)viewDidLoad

   {
    
           songsArray=[[NSArray alloc]init];
    
    songsArray = @[@"Outfit",@"Sardari",@"Time table",@"Salute - Bohemia 190Kbps",@"Taur (Bohemia)",@"Tumhe Apna Banane Ka"];
    
           [musicListTableView reloadData];
    
    
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning


   {
    [super didReceiveMemoryWarning];
       
    // Dispose of any resources that can be recreated.
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
   return songsArray.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
     UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:@"goToMusicPlayer"];
    
   cell.textLabel.text = songsArray[indexPath.row];
    return cell;
    
}



- (void)tableView:(UITableView *)tableView
didSelectRowAtIndexPath:(NSIndexPath *)indexPath

{
//    int randomNumber;
//    
//    NSNumber *index ;
//    randomNumber = (arc4random() %4);
//    [positions addObject:[NSNumber numberWithInteger:randomNumber]];
//    for (int i=0;i<[positions count]; i++)
//    {
//        index=[positions objectAtIndex:i];
//        
//    }


     [self performSegueWithIdentifier:@"musicView" sender:self];
    
    
}


@end
